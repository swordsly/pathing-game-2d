﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

	public GameObject player;
	public GameObject foodfactory;
	public GameObject foreground;
	public GameObject poopSplat;

	private PlayerScript pScript;
	private FoodFactoryScript ffScript;
	private GameObject screenFX;
	private bool isFXRunning = false, isPause = false;
	
	private static int score = 0;
	
	// Use this for initialization
	void Start () {
		pScript = player.GetComponent<PlayerScript>();
		ffScript = foodfactory.GetComponent<FoodFactoryScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI() {
		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUI.skin.label.fontSize = 17;
		GUI.contentColor = Color.white;
		if (GUI.Button(new Rect(Screen.width-30, 0, 30, 30), "||")) {
			isPause = !isPause;
			if (isPause) {
				Time.timeScale = 0f;
			} else {
				Time.timeScale = 1f;
			}
		}
		GUI.Label(new Rect(10, 3, 200, 20), "Score: "+Mathf.RoundToInt(Time.timeSinceLevelLoad).ToString());
	}
	
	public void PoopSplatEffect() {
		if (!isFXRunning) {
			poopSplat.GetComponent<PoopEffectScript>().Activate();
			isFXRunning = true;
		}
	}
	
	public void SetFxRunning(bool flag) {
		isFXRunning = flag;
	}
	
	public static void Lose() {
		score += Mathf.RoundToInt(Time.timeSinceLevelLoad);
		Application.LoadLevel("gameover");
	}
	
	public static void AddScore(int s) {
		score += s;
	}
	
	public static int GetScore() {
		return score;
	}
	
}
