﻿using UnityEngine;
using System.Collections;

public class TrashScript : MonoBehaviour {

//	public float speed = 0.03f;
	public float rotation = 0f;

	private float speed;
	private FoodFactoryScript ffScript;
	
	// Use this for initialization
	void Start () {
		ffScript = GameObject.Find("FoodFactory").GetComponent<FoodFactoryScript>();
		speed = MenuScript.tSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		if (Camera.main.WorldToScreenPoint(gameObject.transform.position).x > Screen.width) {
			Destroy();
		}
	}

	void FixedUpdate () {
		Vector3 position = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		position += Vector3.right*speed*50f*Time.fixedDeltaTime;
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(position);
		position += new Vector3(32f, 32f, 0f);
		gameObject.transform.Rotate(Vector3.back*rotation*50f*Time.fixedDeltaTime);
	}
	
	void OnTriggerEnter2D(Collider2D other) {

	}

	public void Destroy() {
		Destroy(gameObject);
	}
}
