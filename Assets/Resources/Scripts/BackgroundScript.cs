﻿using UnityEngine;
using System.Collections;

public class BackgroundScript : MonoBehaviour {

	public float scrollSpd;

	private Vector3 screenPos;

	// Use this for initialization
	void Start () {
		screenPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		if (screenPos.x > Screen.width) {
			screenPos.x = -Screen.width+4f;
		}
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(screenPos);
	}

	void FixedUpdate () {
		screenPos += Vector3.right*scrollSpd*50f*Time.fixedDeltaTime;
	}
}
