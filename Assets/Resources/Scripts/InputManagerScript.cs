﻿using UnityEngine;
using System.Collections;

public class InputManagerScript : MonoBehaviour {

	public GameObject player;
	public GameObject background;

	private PlayerScript pScript;
	private bool tapMode;
	
	// Use this for initialization
	void Start () {
		pScript = player.GetComponent<PlayerScript>();
		tapMode = MenuScript.tapMode;
//		Vector3 bgPos = Camera.main.WorldToScreenPoint(background.transform.position);
//		bgPos.y -= 40f;
//		background.transform.position = Camera.main.ScreenToWorldPoint(bgPos);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.LoadLevel("main");
		}
	}
	
	void OnDrag(DragGesture gesture) {
		if (!tapMode) {
			pScript.SetTouch(gesture.Fingers[0]);
		}
	}

	void OnTap(TapGesture gesture) {
		if (tapMode) {
			pScript.SetTouch(gesture.Fingers[0]);
		}
	}

	void OnFingerStationary(FingerMotionEvent e) {
		if (tapMode) {
			pScript.SetTouch(e.Finger);
		}
	}
}
