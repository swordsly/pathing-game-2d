﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

	public static bool tapMode;
	public static float pSpeed, fSpeed, tSpeed;

	// Use this for initialization
	void Start () {
		tapMode = true;
		fSpeed = 17f;
		tSpeed = 17f;
		pSpeed = 10f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.Quit();
		}
	}

	void OnGUI() {
		GUI.skin.button.fontSize = 22;
		if (GUI.Button(new Rect(Screen.width/2-80, Screen.height/2, 160, 34), "PLAY")) {
			Application.LoadLevel("game");
		}
		GUI.skin.button.fontSize = 16;
		tSpeed = Mathf.Round(GUI.HorizontalSlider(new Rect(10, Screen.height-70, 100, 40), tSpeed, 3f, 23f));
		GUI.Label(new Rect(120, Screen.height - 70, 60, 30), tSpeed.ToString());
		fSpeed = Mathf.Round(GUI.HorizontalSlider(new Rect(10, Screen.height-30, 100, 40), fSpeed, 3f, 23f));
		GUI.Label(new Rect(120, Screen.height - 30, 60, 30), fSpeed.ToString());
		pSpeed = Mathf.Round(GUI.HorizontalSlider(new Rect(260, Screen.height-30, 100, 30), pSpeed, 4f, 12f));
		GUI.Label(new Rect(370, Screen.height-30, 50, 30), pSpeed.ToString());
	}

}
