﻿using UnityEngine;
using System.Collections.Generic;

public class FoodFactoryScript : MonoBehaviour {

	public AnimationCurve speedUp;

	private float spawnTime, currTime, displayTime;
//	private float lowLimit = 20f, uppLimit = Screen.height-40f-20f;
	private int f = 0, t = 0, cLevel;
	private GameObject lastUnit = null;
	private bool canSpawn = true, up = true, first = true;
	private string[] levels = new string[] {"col_pat1", "col_pat2", "slope_pat1", "slope_pat2", "row_pat1", "row_pat2", "row_pat3"};

	private const float camZ = 15f;
	private const float foodH = 64f, objectH = 64f;

	// Use this for initialization
	void Start () {
		Random.seed = 0;
//		cLevel = Random.Range(0, 7);
		spawnTime = speedUp.Evaluate(0f);
	}
	
	// Update is called once per frame
	void Update () {
		spawnTime -= Time.deltaTime;
		if (canSpawn && spawnTime < 0f) {
//			DecideLevel(cLevel);
//			ReadLevel(levels[4]);
			int r = Random.Range(0, 7);
			if (up) {
				lastUnit = Spawn(r, -50f, Random.Range(230f, 398f));
			} else {
				lastUnit = Spawn(r, -50f, Random.Range(62f, 230f));
			}
			up = !up;
			canSpawn = false;
		}
		Finish();
	}

	void OnGUI () {
		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUI.skin.label.fontSize = 14;
		GUI.contentColor = Color.white;
		GUI.Label(new Rect(10, Screen.height-20, 200, 20), "Time: "+displayTime);
	}

	private GameObject Spawn(int index, float xPos, float yPos) {
		GameObject stuff;
		switch (index) {
		case 1:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Chicken"));
			f++;
			break;
		case 2:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Corn"));
			f++;
			break;
		case 3:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Banana"));
			f++;
			break;
		case 4:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Steak"));
			f++;
			break;
		case 5:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Pepper"));
			f++;
			break;
		case 6:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Icecream"));
			f++;
			break;
		default:
			stuff = (GameObject) Instantiate(Resources.Load("Objects/Medicine"));
			t++;
			break;
		}
		stuff.transform.parent = gameObject.transform;
		stuff.transform.position = Vector3.zero;
		stuff.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(xPos, yPos, camZ));
		return stuff;
	}

	private void Finish() {
		if (lastUnit != null && Camera.main.WorldToScreenPoint(lastUnit.transform.position).x > 0f) {
			currTime += Time.deltaTime;
			spawnTime = speedUp.Evaluate(currTime);
			displayTime = spawnTime;
			canSpawn = true;
			lastUnit = null;
		}
	}

//	private void ReadLevel(string file) {
//		TextAsset text = (TextAsset) Resources.Load("TextFiles/"+file);
//		// content -> info and design
//		string[] content = text.text.Split('|');
//		// info portion
//		string[] info = content[0].Split(',');
//		int min = int.Parse(info[0]);
//		int max = int.Parse(info[1]);
//		int start = Random.Range(min, max)+1;
//		int space = 0; //int.Parse(info[2])-8; //10 -> 2
//		// design portion
//		string[] level = content[1].Split('\n');
//
////		int space = 0;
//		GameObject temp = null;
//		for (int i=0; i<level.Length; i++) {
//			string[] col = level[i].Split('/');
//			for (int j=0; j<col.Length; j++) {
//				if (col[j] == "1") {
//					temp = Spawn(1, (j*(-foodH))-50f, (start*foodH)-34f);
//				} else if (col[j] == "2") {
//					temp = Spawn(2, (j*(-40f))-50f, (start*20f)-(i*space));
//				} else if (col[j] == "0") {
//					temp = Spawn(0, (j*(-objectH))-50f, (start*objectH)-34f);
//				}
//			}
////			start--;
//		}
//		lastUnit = temp;
//	}

//	private void DecideLevel(int l) {
//		ReadLevel(levels[l]);
//		int r;
//		switch (l) {
//		case 0:
//			r = Random.Range(1, 11);
//			if (r%5 == 0 || r%5 == 2) {
//				cLevel = 3;
//			} else if (r%5 == 1 || r%5 == 3) {
//				cLevel = 5;
//			} else {
//				cLevel = 6;
//			}
//			break;
//		case 1:
//			r = Random.Range(1, 11);
//			if (r%5 == 0 || r%5 == 2) {
//				cLevel = 2;
//			} else if (r%5 == 1 || r%5 == 3) {
//				cLevel = 4;
//			} else {
//				cLevel = 6;
//			}
//			break;
//		case 2:
//			r = Random.Range(1, 11);
//			if (r%6 == 0 || r%6 == 2) {
//				cLevel = 1;
//			} else if (r%6 == 1 || r%6 == 3 || r%6 == 5) {
//				cLevel = 5;
//			} else {
//				cLevel = 6;
//			}
//			break;
//		case 3:
//			r = Random.Range(1, 11);
//			if (r%6 == 0 || r%6 == 2) {
//				cLevel = 0;
//			} else if (r%6 == 1 || r%6 == 3 || r%6 == 5) {
//				cLevel = 4;
//			} else {
//				cLevel = 6;
//			}
//			break;
//		case 4:
//			r = Random.Range(1, 11);
//			if (r%4 == 1) {
//				cLevel = 1;
//			} else if (r%4 == 2) {
//				cLevel = 6;
//			} else {
//				cLevel = 5;
//			}
//			break;
//		case 5:
//			r = Random.Range(1, 11);
//			if (r%4 == 1) {
//				cLevel = 0;
//			} else if (r%4 == 2) {
//				cLevel = 6;
//			} else {
//				cLevel = 1;
//			}
//			break;
//		default:
//			cLevel = Random.Range(0,6);
//			break;
//		}
//	}
	
}
