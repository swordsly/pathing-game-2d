using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public GameObject gm;
	
	enum Direction {UP, DOWN, STOP};

	private FingerGestures.Finger f = null;
	private float lowLimit = 30f+32f, uppLimit = Screen.height-50f-32f;
	private AudioSource audio;
//	private bool isEating = false;
//	private Sprite idleState, activeState;
	private Vector3 source, destination, b;
	private Direction direction= Direction.STOP;
	private float cSpeed;
	private BufferScript bScript;

	void Awake() {
		bScript = gameObject.GetComponent<BufferScript>();
		audio = gameObject.GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
//		idleState = Resources.Load<Sprite>("Sprites/ph_reddot");
//		activeState = Resources.Load<Sprite>("Sprites/ph_whitedot");
		
		destination = gameObject.transform.position;
		cSpeed = MenuScript.pSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		source = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		if (f != null) {
			Vector3 position = source;
			if (f.Position.y > position.y) {
//					position += Vector3.up*speed;
					direction = Direction.UP;
				} else if (f.Position.y < position.y) {
//					position += Vector3.down*speed;
					direction = Direction.DOWN;
				}
				position.y = f.Position.y;
			if (position.y > lowLimit && position.y < uppLimit) {
//				gameObject.transform.position = Camera.main.ScreenToWorldPoint(position);
				destination = position;
			}
			if (!f.IsDown) {
				f = null;
			}
		}

//		if (isEating) {
//			stateCD -= Time.deltaTime;
//			if (stateCD < 0f) {
//				animateNormal();
//			}
//		}
	}

	void FixedUpdate() {
		if (source.y != destination.y) {
			Move();
		} else {
			direction = Direction.STOP;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {	
		if (other.gameObject.tag != "Trash") {
			if (!bScript.Add(other.gameObject) && other.gameObject.tag == "Bomb") {
				GameManagerScript.Lose();
			}
		} else if (other.gameObject.tag == "Trash") {
			gm.GetComponent<GameManagerScript>().PoopSplatEffect();
			Destroy(other.gameObject);
		}
	}

	// dest is in screen coordinates
	private void Move() {
		switch(direction) {
			case Direction.UP:
				b = Vector3.up*cSpeed*50f*Time.fixedDeltaTime;
				source += b;
				if (source.y < destination.y) {
					gameObject.transform.position = Camera.main.ScreenToWorldPoint(source);
				}
				break;
			case Direction.DOWN:
				b = Vector3.down*cSpeed*50f*Time.fixedDeltaTime;
				source += b;
				if (source.y > destination.y) {
					gameObject.transform.position = Camera.main.ScreenToWorldPoint(source);
				}
				break;
			default:
				break;

		}
	}

	void OnGUI () {
		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUI.skin.label.fontSize = 14;
		GUI.contentColor = Color.yellow;
		GUI.Label(new Rect(230, Screen.height-20, 200, 20), gameObject.transform.localScale.ToString());
	}

//	private void AnimateNormal() {
//		gameObject.GetComponent<SpriteRenderer>().sprite = idleState;
//		isEating = false;
//		stateCD = 0.2f;
//	}

//	private void AnimateEat() {
//		gameObject.GetComponent<SpriteRenderer>().sprite = activeState;
//		isEating = true;
//	}

	public void PlayAudio() {
		audio.PlayOneShot(audio.clip);
	}
	
	public void Enlarge() {
		Vector3 scaleU = gameObject.transform.localScale;
		scaleU += new Vector3(0.1f, 0.1f);
		gameObject.transform.localScale = scaleU;
	}
	
	public void Shrink() {
		if (gameObject.transform.localScale.x > 1.0f) {
			Vector3 scaleD= gameObject.transform.localScale;
			scaleD -= new Vector3(0.1f, 0.1f);
			gameObject.transform.localScale = scaleD;
		}
	}

	public void SetTouch(FingerGestures.Finger finger) {
		if (f == null) {
			f = finger;
		}
	}
}