using UnityEngine;
using System.Collections;

public class FoodScript : MonoBehaviour {

//	public float speed = 0.03f;
	public float rotation = 0f;
	public int timeToLive = 1600;
	public int fID, cID;
	public bool onHold = false;

	private float speed;
	private FoodFactoryScript ffScript;
	private BufferScript bScript;

	// Use this for initialization
	void Start () {
		ffScript = GameObject.Find("FoodFactory").GetComponent<FoodFactoryScript>();
		bScript = GameObject.Find("Player").GetComponent<BufferScript>();
		speed = MenuScript.fSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		if (Camera.main.WorldToScreenPoint(gameObject.transform.position).x > Screen.width && !onHold) {
			Destroy();
		}
	}

	void FixedUpdate() {
		if (!onHold) {
			Vector3 position = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			position += Vector3.right*speed*50f*Time.fixedDeltaTime;
			gameObject.transform.position = Camera.main.ScreenToWorldPoint(position);
			position += new Vector3(32f, 32f, 0f);
			gameObject.transform.Rotate(Vector3.back*rotation*50f*Time.fixedDeltaTime);
		} else {
			timeToLive--;
			if (timeToLive <= 0) {
				Destroy();
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other) {

	}

	public void SetID(int n) {
		fID = n;
	}

	public void Destroy() {
		if (onHold) {
			if (gameObject.tag == "Bomb") {
				GameManagerScript.Lose();
			}
			GameObject.Find("Player").GetComponent<PlayerScript>().Shrink();
			bScript.ReduceCount();
				
		}
		Destroy(gameObject);
	}

	public void OnHold() {
		onHold = true;
		Vector3 holdingPos = new Vector3(-80f, 0f, 5f);
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(holdingPos);
	}
}
