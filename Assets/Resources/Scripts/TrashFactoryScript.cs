﻿using UnityEngine;
using System.Collections;

public class TrashFactoryScript : MonoBehaviour {
	
	private GameObject lastUnit = null;
	private bool canSpawn = true, up = true;
	private float spawnTime = 0.8f;
	private const float camZ = 15f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		spawnTime -= Time.deltaTime;
		if (canSpawn && spawnTime < 0f) {
			if (up) {
				lastUnit = Spawn(-50f, Random.Range(230f, 398f));
			} else {
				lastUnit = Spawn(-50f, Random.Range(62f, 230f));
			}
			up = !up;
			canSpawn = false;
		}
		Finish();
	}
	
	private GameObject Spawn(float xPos, float yPos) {
		GameObject trash = (GameObject) Instantiate(Resources.Load("Objects/Poop"));
		trash.transform.parent = gameObject.transform;
		trash.transform.position = Vector3.zero;
		trash.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(xPos, yPos, camZ));
		return trash;
	}
	
	private void Finish() {
		if (lastUnit != null && Camera.main.WorldToScreenPoint(lastUnit.transform.position).x > 0f) {
			spawnTime = 1.2f;
			canSpawn = true;
			lastUnit = null;
		}
	}
}
