﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BufferScript : MonoBehaviour {

	private PlayerScript pScript;
	private GameObject[] buffer;
	private List<int> removeBuffer = new List<int>();
	private int size = 6;
	private int current = 0;
	private int count = 0;
	private float purgeCD = 15.0f, purgeTime;
	private bool canPurge = true;
	private string combo = "None";

	void Awake() {
		pScript = gameObject.GetComponent<PlayerScript>();
	}
	
	// Use this for initialization
	void Start () {
		buffer = new GameObject[size];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		if (count == 0) {
			current = 0;
		}
		if (Time.time > purgeTime) {
			canPurge = true;
		}
	}

	void OnGUI () {
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		for (int i=0; i<size; i++) {
			GUI.contentColor = Color.white;
			if (buffer[i] != null) {
				Texture2D temp = buffer[i].GetComponent<SpriteRenderer>().sprite.texture;
				if (GUI.Button(new Rect((Screen.width-100)-(i+1)*40, 0, 40, 40), temp)) {
					Purge(i);
				}
				GUI.contentColor = Color.blue;
				GUI.Label(new Rect((Screen.width-100)-(i+1)*40, 40, 40, 20), buffer[i].GetComponent<FoodScript>().timeToLive.ToString());
			} else {
				GUI.Box(new Rect((Screen.width-100)-(i+1)*40, 0, 40, 40), "");
			}
		}
		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUI.Label(new Rect(150, Screen.height-20, 20, 20), current.ToString());
		GUI.Label(new Rect(170, Screen.height-20, 20, 20), count.ToString());
		GUI.Label(new Rect(190, Screen.height-20, 40, 20), canPurge.ToString());
		GUI.Label(new Rect(190, Screen.height-40, 80, 20), combo);
	}

	private bool CheckLeftMatch() {
		int c = current-1;
		if (buffer[(c+1)%size] != null &&
		    buffer[(c+2)%size] != null) {
			if (buffer[c%size].GetComponent<FoodScript>().fID + buffer[(c+1)%size].GetComponent<FoodScript>().fID + buffer[(c+2)%size].GetComponent<FoodScript>().fID == 11 &&
			    buffer[c%size].GetComponent<FoodScript>().cID + buffer[(c+1)%size].GetComponent<FoodScript>().cID + buffer[(c+2)%size].GetComponent<FoodScript>().cID == 5) {
				//hard combo
				combo = "hard";
				removeBuffer.Add(c);
				removeBuffer.Add(c+1);
				removeBuffer.Add(c+2);
				return true;	
			} else if (buffer[c%size].GetComponent<FoodScript>().cID == 1 &&
			           (buffer[(c+1)%size].GetComponent<FoodScript>().cID == 1 ||
			 			buffer[(c+2)%size].GetComponent<FoodScript>().cID == 1)) {
				int result = buffer[c%size].GetComponent<FoodScript>().fID + buffer[(c+1)%size].GetComponent<FoodScript>().fID + buffer[(c+2)%size].GetComponent<FoodScript>().fID;
				if (result == 6 || result == 9) {
					//meat medium combo
					combo = "medium meat";
					removeBuffer.Add(c);
					removeBuffer.Add(c+1);
					removeBuffer.Add(c+2);
					return true;
				}
			} else if (buffer[c%size].GetComponent<FoodScript>().cID == 2 &&
			           (buffer[(c+1)%size].GetComponent<FoodScript>().cID == 2 ||
			 			buffer[(c+2)%size].GetComponent<FoodScript>().cID == 2)) {
				int result = buffer[c%size].GetComponent<FoodScript>().fID + buffer[(c+1)%size].GetComponent<FoodScript>().fID + buffer[(c+2)%size].GetComponent<FoodScript>().fID;
				if (result == 9 || result == 12) {
					//veggie medium combo
					combo = "medium veg";
					removeBuffer.Add(c);
					removeBuffer.Add(c+1);
					removeBuffer.Add(c+2);
					return true;
				}
			} else if (buffer[c%size].GetComponent<FoodScript>().fID == buffer[(c+1)%size].GetComponent<FoodScript>().fID &&
			           buffer[c%size].GetComponent<FoodScript>().fID == buffer[(c+2)%size].GetComponent<FoodScript>().fID) {
				//trivial combo
				combo = "trivial";
				removeBuffer.Add(c);
				removeBuffer.Add(c+1);
				removeBuffer.Add(c+2);
				return true;
			}
		}
		return false;
	}

	private bool CheckRightMatch() {
		int c = current-1;
		if (buffer[(c-1)%size] != null &&
		    buffer[(c-2)%size] != null) {
			if (buffer[c%size].GetComponent<FoodScript>().fID + buffer[(c-1)%size].GetComponent<FoodScript>().fID + buffer[(c-2)%size].GetComponent<FoodScript>().fID == 11 &&
			    buffer[c%size].GetComponent<FoodScript>().cID + buffer[(c-1)%size].GetComponent<FoodScript>().cID + buffer[(c-2)%size].GetComponent<FoodScript>().cID == 5) {
				//hard combo
				combo = "hard";
				removeBuffer.Add(c);
				removeBuffer.Add(c-1);
				removeBuffer.Add(c-2);
				return true;	
			} else if (buffer[c%size].GetComponent<FoodScript>().cID == 1 &&
			           (buffer[(c-1)%size].GetComponent<FoodScript>().cID == 1 ||
			 			buffer[(c-2)%size].GetComponent<FoodScript>().cID == 1)) {
				int result = buffer[c%size].GetComponent<FoodScript>().fID + buffer[(c-1)%size].GetComponent<FoodScript>().fID + buffer[(c-2)%size].GetComponent<FoodScript>().fID;
				if (result == 6 || result == 9) {
					//meat medium combo
					combo = "medium meat";
					removeBuffer.Add(c);
					removeBuffer.Add(c-1);
					removeBuffer.Add(c-2);
					return true;
				}
			} else if (buffer[c%size].GetComponent<FoodScript>().cID == 2 &&
			           (buffer[(c-1)%size].GetComponent<FoodScript>().cID == 2 ||
			 buffer[(c-2)%size].GetComponent<FoodScript>().cID == 2)) {
				int result = buffer[c%size].GetComponent<FoodScript>().fID + buffer[(c-1)%size].GetComponent<FoodScript>().fID + buffer[(c-2)%size].GetComponent<FoodScript>().fID;
				if (result == 9 || result == 12) {
					//veggie medium combo
					combo = "medium veg";
					removeBuffer.Add(c);
					removeBuffer.Add(c-1);
					removeBuffer.Add(c-2);
					return true;
				}
			} else if (buffer[c%size].GetComponent<FoodScript>().fID == buffer[(c-1)%size].GetComponent<FoodScript>().fID &&
			           buffer[c%size].GetComponent<FoodScript>().fID == buffer[(c-2)%size].GetComponent<FoodScript>().fID) {
				//trivial combo
				combo = "trivial";
				removeBuffer.Add(c);
				removeBuffer.Add(c-1);
				removeBuffer.Add(c-2);
				return true;
			}
		}
		return false;
	}

	private bool CheckSpecialCase() {
		if (current%size == 0) {
			current -= 1;
			if (!CheckLeftMatch()) {
				current += 1;
				return false;
			}
			return true;
		}
		return false;
	}
	
	private void CheckAllMatches() {
		if (CheckRightMatch()) {
			foreach (int i in removeBuffer) {
				Remove(i%size);
			} 
			current = removeBuffer[removeBuffer.Count-1];
			removeBuffer.Clear();
		} else if (CheckLeftMatch()) {
			foreach (int i in removeBuffer) {
				Remove(i%size);
			} 
			current = removeBuffer[0];
			removeBuffer.Clear();
		} else if (CheckSpecialCase()) {
			foreach (int i in removeBuffer) {
				Remove(i%size);
			} 
			current = removeBuffer[0];
			removeBuffer.Clear();
		}
	}
	
	private void Purge(int s) {
		if (canPurge) {
			for (int i=s; i<size; i++) {
				Remove(i);
			}
			canPurge = false;
			purgeTime = Time.time + purgeCD;
			current = s;
		}
	}

	public bool Add(GameObject go) {
		if (buffer[current%size] == null) {
			buffer[current%size] = go;
			go.GetComponent<FoodScript>().OnHold();
			pScript.PlayAudio();
			pScript.Enlarge();
			current++;
			AddCount();
			CheckAllMatches();
			return true;
		}
		return false;
	}

	public bool Remove(int i) {
		if (buffer[i] != null) {
			buffer[i].GetComponent<FoodScript>().Destroy();
			return true;
		}
		return false;
	}
	
	public void AddCount() {
		if (count < size) {
			count++;
		}
	}
	
	public void ReduceCount() {
		if (count > 0) {
			count--;
		}
	}
	
}
