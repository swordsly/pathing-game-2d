﻿using UnityEngine;
using System.Collections;

public class PoopEffectScript : MonoBehaviour {

	public int timeToLive = 300;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (timeToLive > 0) {
			timeToLive--;
		}
		if (timeToLive <= 0) {
			gameObject.GetComponent<SpriteRenderer>().renderer.enabled = false;
			GameObject.Find("GameManager").GetComponent<GameManagerScript>().SetFxRunning(false);
		}
	}
	
	public void Activate() {
		gameObject.GetComponent<SpriteRenderer>().renderer.enabled = true;
		timeToLive = 300;
	}
	
}
