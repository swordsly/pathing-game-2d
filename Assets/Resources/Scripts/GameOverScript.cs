﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		GUI.skin.button.fontSize = 22;
		if (GUI.Button(new Rect(Screen.width/2-80, Screen.height/2, 160, 34), "MAIN MENU")) {
			Application.LoadLevel("main");
		}
		GUI.Label(new Rect(Screen.width/2-80, Screen.height/2+34, 160, 34), "GAME OVER");
		GUI.Label(new Rect(Screen.width/2-80, Screen.height/2+68, 160, 34), "Score: "+GameManagerScript.GetScore());
	}
	
}
